import jetbrains.buildServer.configs.kotlin.*
import jetbrains.buildServer.configs.kotlin.buildFeatures.XmlReport
import jetbrains.buildServer.configs.kotlin.buildFeatures.perfmon
import jetbrains.buildServer.configs.kotlin.buildFeatures.xmlReport
import jetbrains.buildServer.configs.kotlin.buildSteps.script
import jetbrains.buildServer.configs.kotlin.triggers.schedule
import jetbrains.buildServer.configs.kotlin.triggers.vcs

/*
The settings script is an entry point for defining a TeamCity
project hierarchy. The script should contain a single call to the
project() function with a Project instance or an init function as
an argument.

VcsRoots, BuildTypes, Templates, and subprojects can be
registered inside the project using the vcsRoot(), buildType(),
template(), and subProject() methods respectively.

To debug settings scripts in command-line, run the

    mvnDebug org.jetbrains.teamcity:teamcity-configs-maven-plugin:generate

command and attach your debugger to the port 8000.

To debug in IntelliJ Idea, open the 'Maven Projects' tool window (View
-> Tool Windows -> Maven Projects), find the generate task node
(Plugins -> teamcity-configs -> teamcity-configs:generate), the
'Debug' option is available in the context menu for the task.
*/

version = "2023.05"

project {

    buildType(PipelineIaC)
}

object PipelineIaC : BuildType({
    name = "Pipeline IaC"

    vcs {
        root(DslContext.settingsRoot)
    }

    steps {
        script {
            name = "Sonar"
            scriptContent = """
                mvn clean verify sonar:sonar \
                  -Dsonar.qualitygate.wait=true \
                  -Dsonar.projectKey=ReportPortalProject \
                  -Dsonar.projectName='ReportPortalProject' \
                  -Dsonar.host.url=${'$'}SONAR_HOST \
                  -Dsonar.token=${'$'}SONAR_TOKEN
            """.trimIndent()
        }
        script {
            name = "Unit-tests"
            scriptContent = "mvn clean test -P unit-tests"
        }
        script {
            name = "API-tests"
            scriptContent = "mvn clean test -P api-tests"
        }
        script {
            name = "ui-tests"
            scriptContent = """
                mvn clean test -P selenide-tests
                mvn surefire-report:report-only
            """.trimIndent()
        }
        script {
            name = "allure"
            scriptContent = "mvn allure:report"
        }
    }

    triggers {
        vcs {
            branchFilter = "+:main"
        }
        schedule {
            branchFilter = "+:main"
            triggerBuild = always()
        }
    }

    features {
        perfmon {
        }
        xmlReport {
            reportType = XmlReport.XmlReportType.SUREFIRE
            rules = "**/surefire-reports/*.xml"
        }
    }
})
