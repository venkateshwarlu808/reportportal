Feature: Basic demo filter

  Background:
    Given User goes to Filters page

  Scenario: Rename demo filter two times
    When User selects edit "DEMO_FILTER"
    And User renames filter as "The best filter!"
    And Filter "The best filter!" should be displayed
    When User selects edit "The best filter!"
    And User renames filter as "DEMO_FILTER"
    Then Filter "DEMO_FILTER" should be displayed

  Scenario: Demo filter is displayed on Launches page
    When User toggles "DEMO_FILTER" display on launches to "true"
    And User goes to Launches page
    Then Filter "DEMO_FILTER" is displayed

  Scenario: Demo filter is not displayed on Launches page
    When User toggles "DEMO_FILTER" display on launches to "false"
    And User goes to Launches page
    Then Filter "DEMO_FILTER" is not displayed
#      DEPRECATED TEST
#  Scenario: Turn off demo filter sharing
#    When User selects edit "DEMO_FILTER"
#    And User toggles sharing option "false"
#    Then Sharing option should be "false"
#      DEPRECATED TEST
#  Scenario: Turn on demo filter sharing
#    When User selects edit "DEMO_FILTER"
#    And User toggles sharing option "true"
#    Then Sharing option should be "true"