Feature: Add, find and delete new filter with single parameter

  Background:
    Given User goes to Filters page

  Scenario Outline: Delete filter
    When User deletes "<filterName>"
    Then Filter "<filterName>" should be deleted
    Examples:
      | filterName    |
      | Vlad's filter |
      | Filter less   |

  Scenario Outline: Search for new filter
    When User searches for "<filterName>"
    Then Filter "<filterName>" should be displayed
    Examples:
      | filterName    |
      | Vlad's filter |
      | Filter less   |

  Scenario Outline: Add new filter with parameters
    When User clicks Add filter button
    And User specifies filter with parameters: "<filterOptionName>", "<condition>", "<value>"
    And User saves new filter with name "<filterName>"
    When User goes to Filters page
    Then Filter "<filterName>" with parameters "<filterOptionName>", "<condition>", "<value>" is on Filters page

    Examples:
      | filterName    | filterOptionName | condition          | value |
      | Vlad's filter | Product bug      | Equals             | 1     |
      | Filter less   | Passed           | Less than or equal | 2     |