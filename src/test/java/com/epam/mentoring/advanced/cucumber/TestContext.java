package com.epam.mentoring.advanced.cucumber;

import com.epam.mentoring.advanced.managers.DriverManager;
import com.epam.mentoring.advanced.managers.PageObjectManager;

public class TestContext {
    private final DriverManager driverManager;
    private final PageObjectManager pageObjectManager;

    public TestContext() {
        driverManager = new DriverManager();
        pageObjectManager = new PageObjectManager(driverManager.getDriver());
    }

    public DriverManager getDriverManager() {
        return driverManager;
    }

    public PageObjectManager getPageObjectManager() {
        return pageObjectManager;
    }
}
