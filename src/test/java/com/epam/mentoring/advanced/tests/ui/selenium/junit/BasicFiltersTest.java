package com.epam.mentoring.advanced.tests.ui.selenium.junit;

import com.epam.mentoring.advanced.hooks.Hooks;
import com.epam.mentoring.advanced.listeners.TestListener;
import com.epam.mentoring.advanced.pages.LoginPage;
import com.epam.mentoring.advanced.steps.CommonSteps;
import com.epam.mentoring.advanced.steps.FiltersSteps;
import com.epam.mentoring.advanced.steps.LaunchesSteps;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;

@ExtendWith({TestListener.class})
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class BasicFiltersTest extends Hooks {
    FiltersSteps filtersSteps = new FiltersSteps(getDriver());
    LaunchesSteps launchesSteps = new LaunchesSteps(getDriver());
    CommonSteps commonSteps = new CommonSteps(getDriver());

    @BeforeAll
    public static void beforeTest() {
        if (threadCount == 2) {
            LoginPage loginPage = new LoginPage(driverThreadLocal.get());
            loginPage.login(envConfig.user(), envConfig.password());
        }
        System.out.println("Before Test Thread Number Is " + Thread.currentThread().getId());
    }
    @BeforeEach
    public void openFiltersPage() {
        commonSteps.goToFiltersPage();
    }

    @Test
    @DisplayName("User is able to access filters page")
    void accessFilters() {
        filtersSteps.verifyThatUserIsOnFiltersPage();
    }

    @Test
    @DisplayName("Edit demo filter name")
    void editDemoFilterName() {
        filtersSteps.editFilter("DEMO_FILTER");
        filtersSteps.renameSelectedFilter("The best filter!");
        filtersSteps.verifyThatFilterInTheList("The best filter!");
        filtersSteps.editFilter("The best filter!");
        filtersSteps.renameSelectedFilter("DEMO_FILTER");
        filtersSteps.verifyThatFilterInTheList("DEMO_FILTER");
    }
    @Test
    @DisplayName("Demo filter is displayed on Launches page")
    void displayFilterOnLaunches() {
        filtersSteps.toggleDisplayOnLaunches("DEMO_FILTER", "true");
        commonSteps.goToLaunchesPage();
        launchesSteps.verifyThatFilterIsDisplayed("DEMO_FILTER");
    }
    @Test
    @DisplayName("Demo filter is not displayed on Launches page")
    void doNotDisplayFilterOnLaunches() {
        filtersSteps.toggleDisplayOnLaunches("DEMO_FILTER", "false");
        commonSteps.goToLaunchesPage();
        launchesSteps.verifyThatFilterIsNotDisplayed("DEMO_FILTER");
    }
    //DEPRECATED TEST
    @Test
    @DisplayName("DEPRECATED - Turn off demo filter sharing")
    @Disabled("DEPRECATED TEST")
    void toggleShareDemoFilterOFF() {
        filtersSteps.editFilter("DEMO_FILTER");
        filtersSteps.toggleSharing("false");
        filtersSteps.verifySharing("false");
    }
    //DEPRECATED TEST
    @Test
    @DisplayName("DEPRECATED - Turn on demo filter sharing")
    @Disabled("DEPRECATED TEST")
    void toggleShareDemoFilterON() {
        filtersSteps.editFilter("DEMO_FILTER");
        filtersSteps.toggleSharing("true");
        filtersSteps.verifySharing("true");
    }
}
