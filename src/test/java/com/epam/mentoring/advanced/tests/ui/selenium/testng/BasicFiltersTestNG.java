package com.epam.mentoring.advanced.tests.ui.selenium.testng;

import com.epam.mentoring.advanced.hooks.HooksNG;
import com.epam.mentoring.advanced.listeners.TestNGListener;
import com.epam.mentoring.advanced.pages.LoginPage;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

@Listeners(TestNGListener.class)
public class BasicFiltersTestNG extends HooksNG {

    @BeforeTest
    public void beforeTest() {
        if (threadCount == 2) {
            LoginPage loginPage = new LoginPage(getDriver());
            loginPage.login(envConfig.user3(), envConfig.password3());
        }
        System.out.println("Before Test Thread Number Is " + Thread.currentThread().getId());
    }

    @BeforeMethod
    public void openFiltersPage() {
        commonSteps.goToFiltersPage();
    }

    @Test(description = "User is able to access filters page")
    public void accessFilters() {
        filtersSteps.verifyThatUserIsOnFiltersPage();
    }

    @Test(description = "Edit demo filter name")
    public void editDemoFilterName() {
        filtersSteps.editFilter("DEMO_FILTER");
        filtersSteps.renameSelectedFilter("The best filter!");
        filtersSteps.verifyThatFilterInTheList("The best filter!");
        filtersSteps.editFilter("The best filter!");
        filtersSteps.renameSelectedFilter("DEMO_FILTER");
        filtersSteps.verifyThatFilterInTheList("DEMO_FILTER");
    }

    @Test(description = "Demo filter is displayed on Launches page")
    public void displayFilterOnLaunches() {
        filtersSteps.toggleDisplayOnLaunches("DEMO_FILTER", "true");
        commonSteps.goToLaunchesPage();
        launchesSteps.verifyThatFilterIsDisplayed("DEMO_FILTER");
    }
    //
    @Test(description = "Demo filter is not displayed on Launches page")
    public void doNotDisplayFilterOnLaunches() {
        filtersSteps.toggleDisplayOnLaunches("DEMO_FILTER", "false");
        commonSteps.goToLaunchesPage();
        launchesSteps.verifyThatFilterIsNotDisplayed("DEMO_FILTER");
    }

    //DEPRECATED TEST
    @Test(description = "DEPRECATED - Turn off demo filter sharing", enabled = false)
    public void toggleShareDemoFilterOFF() {
        filtersSteps.editFilter("DEMO_FILTER");
        filtersSteps.toggleSharing("false");
        filtersSteps.verifySharing("false");
    }

    //DEPRECATED TEST
    @Test(description = "DEPRECATED - Turn on demo filter sharing", enabled = false)
    public void toggleShareDemoFilterON() {
        filtersSteps.editFilter("DEMO_FILTER");
        filtersSteps.toggleSharing("true");
        filtersSteps.verifySharing("true");
    }

}