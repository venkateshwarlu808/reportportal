package com.epam.mentoring.advanced.tests.ui.selenide;

import com.epam.mentoring.advanced.hooks.HooksSelenide;
import com.epam.mentoring.advanced.listeners.SelenideListener;
import com.epam.mentoring.advanced.steps.selenide.CommonStepsSelenide;
import com.epam.mentoring.advanced.steps.selenide.FiltersStepsSelenide;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

@Listeners({SelenideListener.class})
public class BasicFiltersTestSelenide extends HooksSelenide {
    FiltersStepsSelenide filtersStepsSelenide = new FiltersStepsSelenide();
    CommonStepsSelenide commonStepsSelenide =new CommonStepsSelenide();

    @BeforeMethod
    public void openFiltersPage() {
        commonStepsSelenide.goToFiltersPage();
    }

    @Test(description = "User is able to access filters page")
    public void accessFilters() {
        filtersStepsSelenide.verifyThatUserIsOnFiltersPage();
    }

    @Test(description = "Edit demo filter name")
    public void editDemoFilterName() {
        filtersStepsSelenide.editFilter("DEMO_FILTER");
        filtersStepsSelenide.renameSelectedFilter("The best filter!");
        filtersStepsSelenide.verifyThatFilterInTheList("The best filter!");
        filtersStepsSelenide.editFilter("The best filter!");
        filtersStepsSelenide.renameSelectedFilter("DEMO_FILTER");
        filtersStepsSelenide.verifyThatFilterInTheList("DEMO_FILTER");
    }

    @Test(description = "Demo filter is displayed on Launches page")
    public void displayFilterOnLaunches() {
        filtersStepsSelenide.toggleDisplayOnLaunches("DEMO_FILTER", "true");
        commonStepsSelenide.goToLaunchesPage();
        filtersStepsSelenide.verifyThatFilterIsDisplayed("DEMO_FILTER");
    }
    //
    @Test(description = "Demo filter is not displayed on Launches page")
    public void doNotDisplayFilterOnLaunches() {
        filtersStepsSelenide.toggleDisplayOnLaunches("DEMO_FILTER", "false");
        commonStepsSelenide.goToLaunchesPage();
        filtersStepsSelenide.verifyThatFilterIsNotDisplayed("DEMO_FILTER");
    }
}
