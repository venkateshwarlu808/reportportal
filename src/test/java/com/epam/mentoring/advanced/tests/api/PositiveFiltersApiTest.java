package com.epam.mentoring.advanced.tests.api;

import com.epam.mentoring.advanced.api.controllers.Api;
import com.epam.mentoring.advanced.api.mappers.FilterMapper;
import com.epam.mentoring.advanced.api.models.filter.Filter;
import com.epam.mentoring.advanced.api.models.filter.FilterManager;
import org.testng.annotations.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;



public class PositiveFiltersApiTest {
    Filter filterForCreation = FilterManager.getProductBugFilter();
    Filter filterForPut = FilterManager.getProductBugFilterChanged();

    @Test
    public void getFilters() {
        Api.filters.getFilters()
                .statusCode(200)
                .body("page.totalElements", greaterThanOrEqualTo(0));
    }

    @Test
    public void postFilter() {
        int filterId = Api.filters.postFilter(filterForCreation)
                .statusCode(201).extract().path("id");

        assertThat(filterId, allOf(
                instanceOf(Integer.class),
                greaterThan(0)
        ));

        FilterManager.filterId = filterId;
    }

    @Test(dependsOnMethods = { "postFilter" })
    public void getFilterById() {
        Filter retrievedFilter = Api.filters.getFilterById(FilterManager.filterId)
                .statusCode(200).extract().as(Filter.class);

        assertThat(FilterMapper.mapToPostDto(retrievedFilter),
                equalTo(FilterMapper.mapToPostDto(filterForCreation)));
    }

    @Test(dependsOnMethods = { "getFilterById" })
    public void putFilter() {
        filterForPut.setId(FilterManager.filterId);
        Api.filters.putFilter(filterForPut)
                .statusCode(200)
                .body("message",
                        equalTo("User filter with ID = '" +
                                FilterManager.filterId +
                                "' successfully updated."));
    }

    @Test(dependsOnMethods = { "putFilter" })
    public void deleteFilter() {
        Api.filters.deleteFilter(FilterManager.filterId)
                .statusCode(200)
                .body("message",
                        equalTo("User filter with ID = '" +
                                FilterManager.filterId +
                                "' successfully deleted."));
    }
}