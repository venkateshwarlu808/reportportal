package com.epam.mentoring.advanced.tests.ui.selenium.junit;

import com.epam.mentoring.advanced.hooks.Hooks;
import com.epam.mentoring.advanced.listeners.TestListener;
import com.epam.mentoring.advanced.pages.LoginPage;
import com.epam.mentoring.advanced.steps.CommonSteps;
import com.epam.mentoring.advanced.steps.FiltersSteps;
import com.epam.mentoring.advanced.steps.LaunchesSteps;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;


@ExtendWith({TestListener.class})
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class AdvancedFiltersTest extends Hooks {
    FiltersSteps filtersSteps = new FiltersSteps(getDriver());
    LaunchesSteps launchesSteps = new LaunchesSteps(getDriver());
    CommonSteps commonSteps = new CommonSteps(getDriver());

    @BeforeAll
    public static void beforeTest() {
        if (threadCount == 2) {
            LoginPage loginPage = new LoginPage(driverThreadLocal.get());
            loginPage.login(envConfig.user2(), envConfig.password2());
        }
        System.out.println("Before Test Thread Number Is " + Thread.currentThread().getId());
    }
    @BeforeEach
    public void openFiltersPage() {
        commonSteps.goToFiltersPage();
    }

    @ParameterizedTest
    @Order(1)
    @CsvFileSource(resources = "/data.csv")
    @DisplayName("Add new filter: Product bug")
    void addNewFilter(String filterName, String filterOptionName, String condition, String value) {
        filtersSteps.clickAddFilterButton();

        launchesSteps.clickMoreButton();
        launchesSteps.selectFilterOption(filterOptionName);
        launchesSteps.specifyFilterOption(filterOptionName, condition, value);
//        launchesSteps.specifyNewFilterParameters(filterOptionName, condition, value);

        launchesSteps.saveFilter();
        launchesSteps.enterFilterName(filterName);
        launchesSteps.clickAddButton();
//        launchesSteps.saveNewFilterWithName(filterName);

        commonSteps.goToFiltersPage();

        filtersSteps.verifyThatFilterInTheList(filterName);
        filtersSteps.verifyThatFilterHasSpecifiedOptions(filterName, filterOptionName, condition, value);
//        filtersSteps.verifyThatFilterHasSpecifiedOptions(filterName, filterOptionName, condition, value);
    }
    @ParameterizedTest
    @Order(2)
    @CsvFileSource(resources = "/data.csv")
    @DisplayName("Search for filter")
    void searchForFilter(String filterName, String filterOptionName, String condition, String value) {
        filtersSteps.searchForFilter(filterName);
        filtersSteps.verifyThatFilterInTheList(filterName);
        filtersSteps.verifyThatFilterHasSpecifiedOptions(filterName, filterOptionName, condition, value);
    }
    @ParameterizedTest
    @Order(3)
    @CsvFileSource(resources = "/data.csv")
    @DisplayName("Delete created filter")
    void deleteCreatedFilter(String filterName) {
        filtersSteps.deleteFilterFromTheList(filterName);
        filtersSteps.verifyThatFilterIsNotInTheList(filterName);
    }
}
