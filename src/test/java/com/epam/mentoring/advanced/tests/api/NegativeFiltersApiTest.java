package com.epam.mentoring.advanced.tests.api;

import com.epam.mentoring.advanced.api.controllers.Api;
import com.epam.mentoring.advanced.api.models.filter.Filter;
import com.epam.mentoring.advanced.api.models.filter.FilterManager;
import com.epam.mentoring.advanced.config.ApiConfig;
import org.aeonbits.owner.ConfigFactory;
import org.testng.annotations.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

public class NegativeFiltersApiTest {
    private final ApiConfig apiConfig = ConfigFactory.create(ApiConfig.class);

    int invalidId = -1;

    Filter filterForCreationWithInvalidParameter = FilterManager.getFilterForWithInvalidParameter();
    Filter filterForCreationWithMissingOrder = FilterManager.getFilterWithMissingOrder();
    Filter filterForPutWithInvalidParameter = FilterManager.getFilterForWithInvalidParameter();
    Filter filterForPutWithInvalidId = FilterManager.getFilterWithSpecifiedId(invalidId);

    @Test
    public void getFilterByInvalidId() {
        Api.filters.getFilterById(invalidId)
                .statusCode(404)
                .body("errorCode",
                        equalTo(40421))
                .body("message",
                        equalTo("User filter with ID '"
                                + invalidId
                                + "' not found on project '"
                                + apiConfig.user()
                                + "_personal'. Did you use correct User Filter ID?"));
    }

    @Test
    public void postFilterWithInvalidParameter() {
        Api.filters.postFilter(filterForCreationWithInvalidParameter)
                .statusCode(400)
                .body("errorCode",
                        equalTo(40011))
                .body("message",
                        equalTo("Incorrect filtering parameters. " +
                                filterForPutWithInvalidParameter.getConditions().get(0).getCondition()));
    }

    @Test
    public void postFilterWithMissingOrder() {
        Api.filters.postFilter(filterForCreationWithMissingOrder)
                .statusCode(400)
                .body("errorCode",
                        equalTo(4001))
                .body("message",
                        equalTo("Incorrect Request. [Field 'orders' should not be null.] "));
    }

    @Test
    public void putFilterWithInvalidParameter() {
        Api.filters.putFilter(filterForPutWithInvalidParameter)
                .statusCode(400)
                .body("errorCode",
                        equalTo(40011))
                .body("message",
                        equalTo("Incorrect filtering parameters. " +
                                filterForPutWithInvalidParameter.getConditions().get(0).getCondition()));
    }

    @Test
    public void putFilterWithInvalidId() {
        Api.filters.putFilter(filterForPutWithInvalidId)
                .statusCode(404)
                .body("errorCode",
                        equalTo(40421))
                .body("message",
                        equalTo("User filter with ID '"
                                + filterForPutWithInvalidId.getId()
                                + "' not found on project '"
                                + apiConfig.user()
                                +"_personal'. Did you use correct User Filter ID?"));
    }

    @Test
    public void deleteFilterWithInvalidId() {
        Api.filters.deleteFilter(invalidId)
                .statusCode(404)
                .body("message",
                        equalTo("User filter with ID '"
                                + invalidId
                                + "' not found on project '"
                                + apiConfig.user()
                                +"_personal'. Did you use correct User Filter ID?"));
    }

}
