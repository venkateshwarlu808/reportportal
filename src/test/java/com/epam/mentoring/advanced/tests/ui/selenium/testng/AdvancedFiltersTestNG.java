package com.epam.mentoring.advanced.tests.ui.selenium.testng;

import com.epam.mentoring.advanced.data.ExcelDataProviders;
import com.epam.mentoring.advanced.hooks.HooksNG;
import com.epam.mentoring.advanced.listeners.TestNGListener;
import com.epam.mentoring.advanced.pages.LoginPage;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

@Listeners(TestNGListener.class)
public class AdvancedFiltersTestNG extends HooksNG {

    @BeforeTest
    public void beforeTest() {
        if (threadCount == 2) {
            LoginPage loginPage = new LoginPage(getDriver());
            loginPage.login(envConfig.user4(), envConfig.password4());
        }
        System.out.println("Before Test Thread Number Is " + Thread.currentThread().getId());
    }
    @BeforeMethod
    public void openFiltersPage() {
        commonSteps.goToFiltersPage();
    }

    @Test(description = "Add new filter", dataProvider = "dataFromExcel", dataProviderClass = ExcelDataProviders.class)
    public void addNewFilter(String filterName, String filterOptionName, String condition, String value) {
        filtersSteps.clickAddFilterButton();
        launchesSteps.clickMoreButton();
        launchesSteps.selectFilterOption(filterOptionName);
        launchesSteps.specifyFilterOption(filterOptionName, condition, value);
        launchesSteps.saveFilter();
        launchesSteps.enterFilterName(filterName);
        launchesSteps.clickAddButton();
        commonSteps.goToFiltersPage();
        filtersSteps.verifyThatFilterInTheList(filterName);
        filtersSteps.verifyThatFilterHasSpecifiedOptions(filterName, filterOptionName, condition, value);
    }

    @Test(dependsOnMethods = "addNewFilter", description = "Search for filter", dataProvider = "dataFromExcel", dataProviderClass = ExcelDataProviders.class)
    public void searchForFilter(String filterName, String filterOptionName, String condition, String value) {
        filtersSteps.searchForFilter(filterName);
        filtersSteps.verifyThatFilterInTheList(filterName);
        filtersSteps.verifyThatFilterHasSpecifiedOptions(filterName, filterOptionName, condition, value);
    }

    @Test(dependsOnMethods = "searchForFilter", description = "Delete created filter", dataProvider = "dataFromExcel", dataProviderClass = ExcelDataProviders.class)
    public void deleteCreatedFilter(String filterName, String filterOptionName, String condition, String value) {
        filtersSteps.deleteFilterFromTheList(filterName);
        filtersSteps.verifyThatFilterIsNotInTheList(filterName);
    }

}
