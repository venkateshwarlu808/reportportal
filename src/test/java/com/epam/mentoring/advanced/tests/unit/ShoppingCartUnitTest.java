package com.epam.mentoring.advanced.tests.unit;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import com.epam.mentoring.advanced.app.bo.Product;
import com.epam.mentoring.advanced.app.bo.ShoppingCart;
import com.epam.mentoring.advanced.app.exceptions.ProductNotFoundException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class ShoppingCartUnitTest {
    private ShoppingCart shoppingCart;

    @BeforeEach
    public void setUp() {
        List<Product> products = new ArrayList<>();
        shoppingCart = new ShoppingCart(products);
    }

    @Test
    void addProductToCart_addsProductToEmptyCart() {
        Product product = new Product(1, "Product 1", 10.0, 2.0);

        shoppingCart.addProductToCart(product);

        assertEquals(1, shoppingCart.getProducts().size());
        assertTrue(shoppingCart.getProducts().contains(product));
    }

    @Test
    void addProductToCart_addsProductToNonEmptyCart() {
        Product product1 = new Product(1, "Product 1", 10.0, 2.0);
        Product product2 = new Product(2, "Product 2", 20.0, 1.5);
        shoppingCart.addProductToCart(product1);

        shoppingCart.addProductToCart(product2);

        assertEquals(2, shoppingCart.getProducts().size());
        assertTrue(shoppingCart.getProducts().contains(product1));
        assertTrue(shoppingCart.getProducts().contains(product2));
    }

    @Test
    void removeProductFromCart_removesProductFromNonEmptyCart() {
        Product product1 = new Product(1, "Product 1", 10.0, 2.0);
        Product product2 = new Product(2, "Product 2", 15.0, 1.5);
        List<Product> products = new ArrayList<>();
        products.add(product1);
        products.add(product2);
        ShoppingCart cart = new ShoppingCart(products);

        cart.removeProductFromCart(product2);
        assertFalse(cart.getProducts().stream().anyMatch(p -> p.getId() == 2));
        assertTrue(cart.getProducts().stream().anyMatch(p -> p.getId() == 1));
        assertEquals(1, cart.getProducts().size());
    }

    @Test
    void removeProductFromCart_throwsExceptionWhenProductNotFound() {
        Product product1 = new Product(1, "Product 1", 10.0, 2.0);
        ShoppingCart cart = new ShoppingCart(Collections.singletonList(product1));
        Product product2 = new Product(2, "Product 2", 15.0, 1.5);

        assertThrows(ProductNotFoundException.class, () -> cart.removeProductFromCart(product2));
    }
    @Test
    void getCartTotalPrice_returnsTotalPrice() {
        Product product1 = new Product(1, "Product 1", 10.0, 2.0);
        Product product2 = new Product(2, "Product 2", 15.0, 1.5);
        List<Product> products = new ArrayList<>();
        products.add(product1);
        products.add(product2);
        ShoppingCart cart = new ShoppingCart(products);

        assertEquals(42.5, cart.getCartTotalPrice());
    }

}

