package com.epam.mentoring.advanced.runners;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        features = {"src/test/resources/features"},
        glue = {"com.epam.mentoring.advanced.hooks.cucumber",
                "com.epam.mentoring.advanced.steps"},
        plugin = {"pretty", "summary", "io.qameta.allure.cucumberjvm.AllureCucumberJvm"},
        monochrome = true
)
public class RunCucumberTest {
}
