package com.epam.mentoring.advanced.hooks.cucumber;

import com.epam.mentoring.advanced.config.EnvConfig;
import com.epam.mentoring.advanced.cucumber.TestContext;
import com.epam.mentoring.advanced.pages.*;
import io.cucumber.java.*;
import io.qameta.allure.Allure;
import org.aeonbits.owner.ConfigFactory;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import java.io.ByteArrayInputStream;


public class HooksCucumber {
    protected static final EnvConfig envConfig = ConfigFactory.create(EnvConfig.class);

    TestContext testContext;
    LoginPage loginPage;
    WebDriver driver;

    public HooksCucumber(TestContext context) {
        testContext = context;
    }

    @Before
    public void setup() {
        driver = testContext.getDriverManager().getDriver();
        driver.get(envConfig.endpoint());
        loginPage = testContext.getPageObjectManager().getLoginPage();
        loginPage.login(envConfig.user5(), envConfig.password5());
    }

    @After
    public void tearDown() {
        testContext.getDriverManager().closeDriver();
    }

    @AfterStep
    public void takeScreenshot(Scenario scenario) {
        if (scenario.isFailed()) {
            byte[] screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
            Allure.addAttachment("Screenshot", "image/png", new ByteArrayInputStream(screenshot), "png");
        }
    }

}

