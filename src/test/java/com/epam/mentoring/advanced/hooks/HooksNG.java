package com.epam.mentoring.advanced.hooks;

import com.epam.mentoring.advanced.config.EnvConfig;
import com.epam.mentoring.advanced.pages.*;
import com.epam.mentoring.advanced.steps.CommonSteps;
import com.epam.mentoring.advanced.steps.FiltersSteps;
import com.epam.mentoring.advanced.steps.LaunchesSteps;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.aeonbits.owner.ConfigFactory;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.*;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class HooksNG {
    protected static final EnvConfig envConfig = ConfigFactory.create(EnvConfig.class);
    protected static final int threadCount = envConfig.threads();
    private final ThreadLocal<WebDriver> driverThreadLocal = new ThreadLocal<>();
    protected FiltersSteps filtersSteps;
    protected LaunchesSteps launchesSteps;
    protected CommonSteps commonSteps;

    @BeforeTest
    public void setup() {
        System.out.println("THREADS COUNT NG: " + threadCount);
        System.out.println("HEY HO! THIS IS TESTNG BEFORE TEST!!!");
        WebDriverManager.chromedriver().setup();
        ChromeOptions options = new ChromeOptions();
        options.setHeadless(envConfig.headless());
        driverThreadLocal.set(new ChromeDriver(options));
        if (envConfig.headless().equals(true)) {
            driverThreadLocal.get().manage().window().setSize(new Dimension(2560, 1600));
        } else {
            driverThreadLocal.get().manage().window().maximize();
        }
        driverThreadLocal.get().get(envConfig.endpoint());
        driverThreadLocal.get().manage().timeouts().pageLoadTimeout(10, TimeUnit.SECONDS);
        driverThreadLocal.get().manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);
        if (threadCount == 1) {
            LoginPage loginPage = new LoginPage(driverThreadLocal.get());
            loginPage.login(envConfig.user3(), envConfig.password3());
        }
//        initializeDemoData();
        filtersSteps = new FiltersSteps(driverThreadLocal.get());
        launchesSteps = new LaunchesSteps(driverThreadLocal.get());
        commonSteps = new CommonSteps(driverThreadLocal.get());
    }

    @AfterTest
    public void tearDown() {
        driverThreadLocal.get().close();
        driverThreadLocal.get().quit();
    }

    public WebDriver getDriver() {
        return driverThreadLocal.get();
    }

    public void initializeDemoData() {
        Map<String, Boolean> isDemoDataPresent = new HashMap<String, Boolean>();

        DashboardsPage dashboardsPage = new DashboardsPage(driverThreadLocal.get());
        LaunchesPage launchesPage = new LaunchesPage(driverThreadLocal.get());
        FiltersPage filtersPage = new FiltersPage(driverThreadLocal.get());
        SettingsPage settingsPage = new SettingsPage(driverThreadLocal.get());
        Sidebar sidebar = new Sidebar(driverThreadLocal.get());

        sidebar.goToDashboardsPage();
        isDemoDataPresent.put("Dashboard", dashboardsPage.checkIfDashboardPresent("DEMO DASHBOARD"));
        sidebar.goToLaunchesPage();
        isDemoDataPresent.put("Launches", launchesPage.checkIfLaunchesPresent());
        sidebar.goToFiltersPage();
        isDemoDataPresent.put("Filters", filtersPage.isFilterInTheList("DEMO_FILTER"));
        System.out.println(isDemoDataPresent);

        if (!isDemoDataPresent.get("Dashboard") ||
                !isDemoDataPresent.get("Launches") ||
                !isDemoDataPresent.get("Filters")) {
            isDemoDataPresent.forEach((dataName, isPresent) ->
                    {
                        System.out.println("Dataname: " + dataName);
                        System.out.println("isPresent: " + isPresent);
                        if (isPresent) {
                            switch (dataName) {
                                case "Dashboard" :
                                    sidebar.goToDashboardsPage();
                                    dashboardsPage.deleteDashboard("DEMO DASHBOARD");
                                case "Launches" :
                                    sidebar.goToLaunchesPage();
                                    launchesPage.deleteAllLaunches();
                                case "Filters" :
                                    sidebar.goToFiltersPage();
                                    filtersPage.deleteFilterFromTheList("DEMO_FILTER");
                            }

                        }
                    }
            );
            sidebar.goToSettingsPage();
            settingsPage.generateDemoData();
        }

    }
}
