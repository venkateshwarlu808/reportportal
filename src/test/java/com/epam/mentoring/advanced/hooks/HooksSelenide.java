package com.epam.mentoring.advanced.hooks;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.WebDriverRunner;
import com.codeborne.selenide.logevents.SelenideLogger;
import com.epam.mentoring.advanced.config.EnvConfig;
import com.epam.mentoring.advanced.managers.DriverManager;
import com.epam.mentoring.advanced.pages.selenide.LoginPageSelenide;
import io.qameta.allure.selenide.AllureSelenide;
import org.aeonbits.owner.ConfigFactory;
import org.testng.ITestContext;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;

import static com.codeborne.selenide.Selenide.open;

public class HooksSelenide {
    protected static final EnvConfig envConfig = ConfigFactory.create(EnvConfig.class);


    @BeforeTest
    public void setup(ITestContext context) {
        String currentTestName = context.getCurrentXmlTest().getName();

        switch (envConfig.host()) {
            case "sauce":
                DriverManager.setTestName(currentTestName);
                DriverManager.setupSauceLabsDriver();
                break;
            case "selenoid":
                DriverManager.setupSelenoidDriver();
                break;
            default:
                DriverManager.setupSelenideDriver();
                break;
        }

        SelenideLogger.addListener("AllureSelenide", new AllureSelenide().screenshots(true));

        open(envConfig.endpoint());

        LoginPageSelenide loginPageSelenide = new LoginPageSelenide();
        loginPageSelenide.login(envConfig.selenideUser(), envConfig.selenidePassword());
    }

    @AfterTest
    public void tearDown(ITestContext context) {
        if(envConfig.host().contentEquals("sauce")) {
            boolean testPassed = context.getFailedTests().size() == 0;
            String status = testPassed ? "passed" : "failed";
            WebDriverRunner.driver().executeJavaScript("sauce:job-result=" + status);
        }

        Selenide.closeWebDriver();
    }
}
