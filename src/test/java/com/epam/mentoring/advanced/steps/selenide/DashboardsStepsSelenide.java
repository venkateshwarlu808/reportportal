package com.epam.mentoring.advanced.steps.selenide;

import com.epam.mentoring.advanced.pages.selenide.DashboardsPageSelenide;
import org.testng.Assert;

import static com.codeborne.selenide.Condition.text;

public class DashboardsStepsSelenide {

    DashboardsPageSelenide dashboardsPageSelenide = new DashboardsPageSelenide();

    public void changeWidgetSize(String widgetName, int xOffset, int yOffset) {
        dashboardsPageSelenide.changeWidgetSize(widgetName, xOffset, yOffset);
    }
    public void moveWidget(String widgetName, int xOffset, int yOffset) {
        dashboardsPageSelenide.moveWidget(widgetName, xOffset, yOffset);
    }
public void verifyThatWidgetSizeIsChanged() {
    Assert.assertTrue(dashboardsPageSelenide.isWidgetSizeChanged());
}
public void verifyThatWidgetLocationIsChanged() {
    Assert.assertTrue(dashboardsPageSelenide.isWidgetMoved());
}

public void verifyThatDashboardIsOpened(String dashboardName) {
    dashboardsPageSelenide.dashboardTitle.shouldHave(text(dashboardName));
}

public void selectDashboardFromTheList(String dashboardName) {
    dashboardsPageSelenide.selectDashboardFromTheList(dashboardName);
}

}
