package com.epam.mentoring.advanced.steps.selenide;

import com.epam.mentoring.advanced.pages.selenide.SidebarSelenide;


public class CommonStepsSelenide {
    SidebarSelenide sidebarSelenide;

    public CommonStepsSelenide() {
        this.sidebarSelenide = new SidebarSelenide();
    }



    public void goToFiltersPage() { sidebarSelenide.goToFiltersPage(); }

    public void goToLaunchesPage() { sidebarSelenide.goToLaunchesPage(); }

    public void goToDashboardsPage() { sidebarSelenide.goToDashboardsPage(); }
}
