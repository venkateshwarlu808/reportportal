package com.epam.mentoring.advanced.steps.selenide;

import com.epam.mentoring.advanced.pages.selenide.LaunchesPageSelenide;
import org.junit.jupiter.api.Assertions;

public class LaunchesStepsSelenide {
    LaunchesPageSelenide launchesPageSelenide = new LaunchesPageSelenide();
    public void specifyNewFilterParameters(String filterOptionName, String condition, String value) {
        launchesPageSelenide.clickMoreButton();
        launchesPageSelenide.selectFilterOptionFromTheList(filterOptionName);
        launchesPageSelenide.specifyFilterOption(filterOptionName, condition, value);
    }

    public void saveNewFilterWithName(String filterName) {
        launchesPageSelenide.clickSaveFilterButton();
        launchesPageSelenide.enterFilterName(filterName);
        launchesPageSelenide.clickAddButton();
    }

    public void clickAddFilterButton() {
        launchesPageSelenide.clickAddFilterButton();
    }
    public void verifyThatFilterIsDisplayed(String filterName) { Assertions.assertTrue(launchesPageSelenide.isFilterDisplayedOnLaunches(filterName)); }

    public void verifyThatFilterIsNotDisplayed(String filterName) { Assertions.assertFalse(launchesPageSelenide.isFilterDisplayedOnLaunches(filterName)); }


    public void selectFilterOption(String filterOptionName) { launchesPageSelenide.selectFilterOptionFromTheList(filterOptionName); }

    public void clickMoreButton() { launchesPageSelenide.clickMoreButton(); }
    public void saveFilter() { launchesPageSelenide.clickSaveFilterButton(); }
    public void enterFilterName(String filterName) { launchesPageSelenide.enterFilterName(filterName); }
    public void clickAddButton() { launchesPageSelenide.clickAddButton(); }
    public void specifyFilterOption(String filterOptionName, String condition, String value) { launchesPageSelenide.specifyFilterOption(filterOptionName, condition, value); }


}
