package com.epam.mentoring.advanced.steps;

import com.epam.mentoring.advanced.cucumber.TestContext;
import com.epam.mentoring.advanced.pages.LaunchesPage;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import org.junit.jupiter.api.Assertions;
import org.openqa.selenium.WebDriver;

public class LaunchesSteps {
    TestContext testContext;
    LaunchesPage launchesPage;
    public LaunchesSteps(TestContext context) {
        testContext = context;
        launchesPage = testContext.getPageObjectManager().getLaunchesPage();
    }
    public LaunchesSteps(WebDriver driver) {
        this.launchesPage = new LaunchesPage(driver);
    }



    @And("User specifies filter with parameters: {string}, {string}, {string}")
    public void specifyNewFilterParameters(String filterOptionName, String condition, String value) {
        launchesPage.clickMoreButton();
        launchesPage.selectFilterOptionFromTheList(filterOptionName);
        launchesPage.specifyFilterOption(filterOptionName, condition, value);
    }

    @And("User saves new filter with name {string}")
    public void saveNewFilterWithName(String filterName) {
        launchesPage.clickSaveFilterButton();
        launchesPage.enterFilterName(filterName);
        launchesPage.clickAddButton();
    }

    @Then("Filter {string} is displayed")
    public void verifyThatFilterIsDisplayed(String filterName) { Assertions.assertTrue(launchesPage.isFilterDisplayedOnLaunches(filterName)); }

    @Then("Filter {string} is not displayed")
    public void verifyThatFilterIsNotDisplayed(String filterName) { Assertions.assertFalse(launchesPage.isFilterDisplayedOnLaunches(filterName)); }


    public void selectFilterOption(String filterOptionName) { launchesPage.selectFilterOptionFromTheList(filterOptionName); }

    public void clickMoreButton() { launchesPage.clickMoreButton(); }
    public void saveFilter() { launchesPage.clickSaveFilterButton(); }
    public void enterFilterName(String filterName) { launchesPage.enterFilterName(filterName); }
    public void clickAddButton() { launchesPage.clickAddButton(); }
    public void specifyFilterOption(String filterOptionName, String condition, String value) { launchesPage.specifyFilterOption(filterOptionName, condition, value); }




}
