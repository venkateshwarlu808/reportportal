package com.epam.mentoring.advanced.steps;

import com.epam.mentoring.advanced.cucumber.TestContext;
import com.epam.mentoring.advanced.pages.Sidebar;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import org.openqa.selenium.WebDriver;


public class CommonSteps {
    TestContext testContext;
    Sidebar sidebar;

    public CommonSteps(TestContext context) {
        testContext = context;
        sidebar = testContext.getPageObjectManager().getSidebar();
    }

    public CommonSteps(WebDriver driver) {
        this.sidebar = new Sidebar(driver);
    }



    @Given("User goes to Filters page")
    public void goToFiltersPage() { sidebar.goToFiltersPage(); }

    @And("User goes to Launches page")
    public void goToLaunchesPage() { sidebar.goToLaunchesPage(); }
}
