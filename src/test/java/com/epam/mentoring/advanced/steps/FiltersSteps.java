package com.epam.mentoring.advanced.steps;

import com.epam.mentoring.advanced.cucumber.TestContext;
import com.epam.mentoring.advanced.pages.FiltersPage;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.jupiter.api.Assertions;
import org.openqa.selenium.WebDriver;


public class FiltersSteps {

    TestContext testContext;
    FiltersPage filtersPage;

    public FiltersSteps(TestContext context) {
        testContext = context;
        filtersPage = testContext.getPageObjectManager().getFiltersPage();
    }

    public FiltersSteps(WebDriver driver) {
        this.filtersPage = new FiltersPage(driver);
    }


    @When("User selects edit {string}")
    public void editFilter(String filterName) { filtersPage.editFilter(filterName);}

    @When("User clicks Add filter button")
    public void clickAddFilterButton() { filtersPage.clickAddNewFilterButton(); }

    @And("User searches for {string}")
    public void searchForFilter(String filterName) { filtersPage.searchForFilter(filterName); }

    @And("User renames filter as {string}")
    public void renameSelectedFilter(String newFilterName) { filtersPage.renameSelectedFilter(newFilterName); }

    @When("User deletes {string}")
    public void deleteFilterFromTheList(String filterName) { filtersPage.deleteFilterFromTheList(filterName); }

    @When("User toggles {string} display on launches to {string}")
    public void toggleDisplayOnLaunches(String filterName, String value) {
        boolean parsedValue = Boolean.parseBoolean(value);
        filtersPage.toggleDisplayOnLaunches(filterName, parsedValue);
    }

    @Then("Filter {string} with parameters {string}, {string}, {string} is on Filters page")
    public void verifyThatFilterHasSpecifiedOptions(String filterName, String filterOptionName, String condition, String value) {
        Assertions.assertTrue(filtersPage.verifyFilterOptions(filterName, filterOptionName, condition, value));
    }
    @Then("Filter {string} should be deleted")
    public void verifyThatFilterIsNotInTheList(String filterName) { Assertions.assertFalse(filtersPage.isFilterInTheList(filterName)); }

    @Then("Filter {string} should be displayed")
    public void verifyThatFilterInTheList(String filterName) { Assertions.assertTrue(filtersPage.isFilterInTheList(filterName)); }

    @Then("User is on Filters page")
    public void verifyThatUserIsOnFiltersPage() { Assertions.assertTrue(filtersPage.isUserOnFiltersPage()); }

    @And("User toggles sharing option {string}")
    public void toggleSharing(String sharingOption) {
        boolean parsedValue = Boolean.parseBoolean(sharingOption);
        filtersPage.toggleSharing(parsedValue);
    }

    @Then("Sharing option should be {string}")
    public void verifySharing(String sharingOption) {
        boolean parsedValue = Boolean.parseBoolean(sharingOption);
        Assertions.assertEquals(parsedValue, filtersPage.isSharedTickDisplayed());
    }

}