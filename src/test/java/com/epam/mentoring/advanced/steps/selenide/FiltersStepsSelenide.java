package com.epam.mentoring.advanced.steps.selenide;

import com.epam.mentoring.advanced.pages.selenide.FiltersPageSelenide;
import com.epam.mentoring.advanced.pages.selenide.LaunchesPageSelenide;
import org.junit.jupiter.api.Assertions;

public class FiltersStepsSelenide {
    FiltersPageSelenide filtersPageSelenide;
    LaunchesPageSelenide launchesPageSelenide;
    public FiltersStepsSelenide() {
        this.filtersPageSelenide = new FiltersPageSelenide();
        this.launchesPageSelenide = new LaunchesPageSelenide();
    }


    public void verifyThatFilterIsDisplayed(String filterName) { Assertions.assertTrue(launchesPageSelenide.isFilterDisplayedOnLaunches(filterName)); }

    public void verifyThatFilterIsNotDisplayed(String filterName) { Assertions.assertFalse(launchesPageSelenide.isFilterDisplayedOnLaunches(filterName)); }

    public void editFilter(String filterName) { filtersPageSelenide.editFilter(filterName);}

    public void clickAddFilterButton() { filtersPageSelenide.clickAddNewFilterButton(); }

    public void searchForFilter(String filterName) { filtersPageSelenide.searchForFilter(filterName); }

    public void renameSelectedFilter(String newFilterName) { filtersPageSelenide.renameSelectedFilter(newFilterName); }

    public void deleteFilterFromTheList(String filterName) { filtersPageSelenide.deleteFilterFromTheList(filterName); }

    public void toggleDisplayOnLaunches(String filterName, String value) {
        boolean parsedValue = Boolean.parseBoolean(value);
        filtersPageSelenide.toggleDisplayOnLaunches(filterName, parsedValue);
    }

    public void verifyThatFilterHasSpecifiedOptions(String filterName, String filterOptionName, String condition, String value) {
        Assertions.assertTrue(filtersPageSelenide.verifyFilterOptions(filterName, filterOptionName, condition, value));
    }
    public void verifyThatFilterIsNotInTheList(String filterName) {
        Assertions.assertFalse(filtersPageSelenide.isFilterInTheList(filterName)); }

    public void verifyThatFilterInTheList(String filterName) { Assertions.assertTrue(filtersPageSelenide.isFilterInTheList(filterName)); }

    public void verifyThatUserIsOnFiltersPage() { Assertions.assertTrue(filtersPageSelenide.isUserOnFiltersPage()); }

}
