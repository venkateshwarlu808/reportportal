package com.epam.mentoring.advanced.listeners;

import com.epam.mentoring.advanced.hooks.Hooks;
import io.qameta.allure.Attachment;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.api.extension.TestWatcher;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

public class TestListener implements TestWatcher {

    @Attachment(value = "Screenshot", type = "image/png")
    private byte[] takeScreenshot(WebDriver driver) {
        return ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
    }
    @Override
    public void testFailed(ExtensionContext context, Throwable cause) {
        Object testInstance = context.getRequiredTestInstance();
        if (testInstance instanceof Hooks) {
            WebDriver driver = ((Hooks) testInstance).getDriver();
            if (driver != null) {
                takeScreenshot(driver);
            }
        }
    }
}
