package com.epam.mentoring.advanced.utils;

import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

public class ExcelReader {
    private final String excelFilePath;
    private XSSFSheet sheet;
    private XSSFWorkbook workBook;
    private final String sheetName;

    public ExcelReader(String excelFilePath, String sheetName) throws IOException {
        this.excelFilePath = excelFilePath;
        this.sheetName = sheetName;
        File file = new File(excelFilePath);
        try (FileInputStream fileInputStream = new FileInputStream(file)) {
            workBook = new XSSFWorkbook(fileInputStream);
            sheet = workBook.getSheet(sheetName);
        } catch (IOException e) {
            throw new IOException("Something wrong with file");
        }
    }

    private String cellToString(XSSFCell cell) throws Exception {
        Object result = null;
        CellType type = cell.getCellType();
        switch (type){
            case NUMERIC:
                result = (int)cell.getNumericCellValue();
                break;
            case STRING:
                result = cell.getStringCellValue();
                break;
            case FORMULA:
                result = cell.getCellFormula();
                break;
            case BLANK:
                result = "";
                break;
            default: throw new Exception("Something wrong with cell");
        }
        return result.toString();
    }

    private int xlsxCountColumn(){
        return sheet.getRow(0).getLastCellNum();
    }

    private int xlsxCountRow(){
        return sheet.getLastRowNum() + 1;
    }

    public String[][] getSheetData() throws Exception {
        File file = new File(excelFilePath);
        try (FileInputStream fileInputStream = new FileInputStream(file)) {
            workBook = new XSSFWorkbook(fileInputStream);
            sheet = workBook.getSheet(sheetName);
        } catch (IOException e) {
            throw new IOException("Something wrong with file");
        }
        int numberOfColumn = xlsxCountColumn();
        int numberOfRows = xlsxCountRow();
        String[][] data = new String[numberOfRows-1][numberOfColumn];
        for(int i = 1; i<numberOfRows; i++){
            for(int j = 0; j<numberOfColumn; j++){
                XSSFRow row = sheet.getRow(i);
                XSSFCell cell = row.getCell(j);
                String value = cellToString(cell);
                data[i-1][j] = value;
            }
        }
        return data;
    }
}
