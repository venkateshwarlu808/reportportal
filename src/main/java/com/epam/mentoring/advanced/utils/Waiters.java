package com.epam.mentoring.advanced.utils;

import com.epam.mentoring.advanced.pages.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import java.util.List;

public class Waiters {
    WebDriverWait webDriverWait;


    public Waiters(WebDriver driver, Long timeOut) {
        this.webDriverWait = new WebDriverWait(driver, timeOut);
    }
    public void waitingForTextToBePresent(WebElement element, String waitingForThisMsg) {
        webDriverWait.until(ExpectedConditions.textToBePresentInElement(element, waitingForThisMsg));
    }

    public void waitingForElementToBeDisplayed(WebElement element) {
        webDriverWait.until(ExpectedConditions.visibilityOf(element));
    }

    public void waitingForElementToBeClickable(WebElement element) {
        webDriverWait.until(ExpectedConditions.elementToBeClickable(element));
    }

    public void waitingForElementsInTheList(List<WebElement> listOfElements) {
        webDriverWait.until(ExpectedConditions.visibilityOfAllElements(listOfElements));
    }

    public void waitingForElementToBeGone(WebElement element) {
        try {
            webDriverWait.until(ExpectedConditions.not(ExpectedConditions.presenceOfElementLocated(By.xpath(getLocatorFromWebElement(element)))));
        } catch (Exception e) {
            BasePage.logger.info("presence of element throws error");
        }

    }

    private String getLocatorFromWebElement(WebElement element) {
        String elementString = element.toString();
        int arrowIndex = elementString.indexOf("->");
        if (arrowIndex != -1) {
            int closingBracketIndex = elementString.indexOf("]", arrowIndex);
            if (closingBracketIndex != -1) {
                return elementString.substring(arrowIndex + 2, closingBracketIndex);
            }
            return "";
        }
        return elementString;
    }
}
