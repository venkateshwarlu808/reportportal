package com.epam.mentoring.advanced.config;

import org.aeonbits.owner.Config;
import org.aeonbits.owner.Config.Sources;
import org.aeonbits.owner.Config.LoadPolicy;

import static org.aeonbits.owner.Config.LoadType.MERGE;

@LoadPolicy(MERGE)
@Sources({"system:properties",
        "system:env",
        "classpath:configs/env/EnvConfig.properties"})

public interface ApiConfig extends Config {
    @Key("api.endpoint")
    @DefaultValue("http://localhost:8080/api/v1/")
    String apiEndpoint();

    @Key("auth.endpoint")
    @DefaultValue("https://localhost:8080/uat/sso/oauth/token")
    String authEndpoint();

    @Key("auth.basic.header")
    @DefaultValue("Basic dWk6dWltYW4=")
    String authBasicHeader();

    @Key("RP_USER")
    @DefaultValue("superadmin")
    String user();

    @Key("RP_PASSWORD")
    @DefaultValue("erebus")
    String password();

    @Key("token")
    String token();
}
