package com.epam.mentoring.advanced.api.dtos;

import com.epam.mentoring.advanced.api.models.filter.Condition;
import com.epam.mentoring.advanced.api.models.filter.Order;
import lombok.Data;

import java.util.List;
@Data
public class PostFilterDto {
    private String name;
    private String description;
    private boolean share;
    private String type;
    private List<Condition> conditions;
    private List<Order> orders;
}
