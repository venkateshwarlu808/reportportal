package com.epam.mentoring.advanced.api.mappers;

import com.epam.mentoring.advanced.api.dtos.PostFilterDto;
import com.epam.mentoring.advanced.api.models.filter.Filter;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.modelmapper.ModelMapper;

import java.util.List;
import java.util.stream.Collectors;

public class FilterMapper {
        private static final ModelMapper modelMapper = new ModelMapper();
    private FilterMapper() {
        throw new IllegalStateException("Utility class");
    }

    static {
        modelMapper.typeMap(Filter.class, PostFilterDto.class)
                .addMappings(m -> m.skip(PostFilterDto::setType))
                .setPostConverter(ctx -> {
                    Filter source = ctx.getSource();
                    PostFilterDto destination = ctx.getDestination();
                    destination.setType(source.getType().toLowerCase());
                    return destination;
                });
    }
        public static PostFilterDto mapToPostDto(Filter filter) {
            return modelMapper.map(filter, PostFilterDto.class);
        }

        public static Filter mapToFilter(PostFilterDto postFilterDto) {
            return modelMapper.map(postFilterDto, Filter.class);
        }
        public static <T> T map(Filter filter, Class<T> clazz) {
            return modelMapper.map(filter, clazz);
        }
    }
