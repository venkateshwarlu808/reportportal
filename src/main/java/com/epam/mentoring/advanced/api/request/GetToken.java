package com.epam.mentoring.advanced.api.request;

import com.epam.mentoring.advanced.config.ApiConfig;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import org.aeonbits.owner.ConfigFactory;

public class GetToken {
    private static final ApiConfig apiConfig = ConfigFactory.create(ApiConfig.class);

    private GetToken() {
        throw new IllegalStateException("Utility class");
    }

    public static String getAuthToken() {
        Response response = RestAssured.given()
                .header("Authorization", apiConfig.authBasicHeader())
                .contentType(ContentType.URLENC)
                .formParam("grant_type", "password")
                .formParam("username", apiConfig.user())
                .formParam("password", apiConfig.password())
                .post(apiConfig.authEndpoint());
        return response.jsonPath().getString("access_token");
    }


}
