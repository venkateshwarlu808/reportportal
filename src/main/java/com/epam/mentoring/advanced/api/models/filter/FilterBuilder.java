package com.epam.mentoring.advanced.api.models.filter;

import java.util.List;

public class FilterBuilder {
    private final Filter filter;


    public FilterBuilder setId(int id) {
        filter.setId(id);
        return this;
    }

    public FilterBuilder setName(String name) {
        filter.setName(name);
        return this;
    }

    public FilterBuilder setOwner(String owner) {
        filter.setOwner(owner);
        return this;
    }

    public FilterBuilder setDescription(String description) {
        filter.setDescription(description);
        return this;
    }

    public FilterBuilder setShare(boolean share) {
        filter.setShare(share);
        return this;
    }

    public FilterBuilder setType(String type) {
        filter.setType(type);
        return this;
    }

    public FilterBuilder setConditions(List<Condition> conditions) {
        filter.setConditions(conditions);
        return this;
    }

    public FilterBuilder setOrders(List<Order> orders) {
        filter.setOrders(orders);
        return this;
    }



    public FilterBuilder() {
        filter = new Filter();
    }


    public Filter build() {
        return filter;
    }
}
