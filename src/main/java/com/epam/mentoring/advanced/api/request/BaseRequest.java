package com.epam.mentoring.advanced.api.request;

import com.epam.mentoring.advanced.config.ApiConfig;
import io.qameta.allure.restassured.AllureRestAssured;
import io.restassured.response.ValidatableResponse;
import io.restassured.specification.RequestSpecification;
import org.aeonbits.owner.ConfigFactory;

import java.util.HashMap;
import java.util.Map;

import static io.restassured.RestAssured.given;

public class BaseRequest {
    private final ApiConfig apiConfig = ConfigFactory.create(ApiConfig.class);
    private static final String AUTH_TOKEN;
    protected String method;
    protected String url = "";
    protected Map<String, String> headers = new HashMap<>();
    protected Map<String, String> pathParams = new HashMap<>();
    protected Map<String, String> queryParams = new HashMap<>();
    protected Object body;

    static {
        AUTH_TOKEN = GetToken.getAuthToken();
    }

    protected BaseRequest() {
        headers.put("Authorization", "bearer " + AUTH_TOKEN);
        headers.put("Content-Type", "application/json");
    }

    public ValidatableResponse execute() {
        RequestSpecification request = given().log().all()
                .filters(new AllureRestAssured())
                .baseUri(apiConfig.apiEndpoint() + apiConfig.user() + "_PERSONAL")
                .headers(headers);
        if (body != null) {
            request.body(body);
        }
        if (pathParams != null) {
            request.pathParams(pathParams);
        }
        if (queryParams != null) {
            request.queryParams(queryParams);
        }

        return request.when()
                .request(method, url)
                .then().log().all();
    }
}
