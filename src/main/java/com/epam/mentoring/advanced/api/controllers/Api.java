package com.epam.mentoring.advanced.api.controllers;

public class Api {
    private Api() {
        throw new IllegalStateException("Utility class");
    }
    public static final FilterController filters = new FilterController();
}
