package com.epam.mentoring.advanced.api.models.filter;
import lombok.Data;

import java.util.List;
@Data
public class Filter {
    private int id;
    private String name;
    private String owner;
    private String description;
    private boolean share;
    private String type;
    private List<Condition> conditions;
    private List<Order> orders;
}