package com.epam.mentoring.advanced.api.models.filter;

import lombok.Data;
@Data
public class Order {
    private boolean isAsc;
    private String sortingColumn;

    public void setIsAsc(boolean isAsc) {
        this.isAsc = isAsc;
    }
}
