package com.epam.mentoring.advanced.api.request.filter;

import com.epam.mentoring.advanced.api.request.BaseRequest;

public class GetFilterById extends BaseRequest {
    public GetFilterById(int id) {
        method = "GET";
        url = "/filter/" + id;
    }
}
