package com.epam.mentoring.advanced.api.models.filter;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

public class FilterManager {
    public static int filterId;
    private static final String NAME = "ProductBugFilter";
    private static final String CHANGED_NAME = "ProductBugFilterChanged";
    private static final String FILTERING_FIELD = "statistics$defects$product_bug$total";
    private static final String TYPE = "launch";
    private static final String SORTING_COLUMN = "number";
    private static final String DESCRIPTION = "some description";

    private FilterManager() {
        throw new IllegalStateException("Utility class");
    }

    public static Filter getProductBugFilter() {
        Condition condition = new Condition();
        condition.setCondition("eq");
        condition.setFilteringField(FILTERING_FIELD);
        condition.setValue("5");
        List<Condition> conditions = new ArrayList<>();
        conditions.add(condition);

        Order order1 = new Order();
        order1.setIsAsc(false);
        order1.setSortingColumn(SORTING_COLUMN);
        List<Order> orders = new ArrayList<>();
        orders.add(order1);

        return new FilterBuilder()
                .setName(NAME + "_" + Instant.now().getEpochSecond())
                .setType(TYPE)
                .setShare(false)
                .setDescription(DESCRIPTION)
                .setConditions(conditions)
                .setOrders(orders)
                .build();
    }

    public static Filter getFilterForWithInvalidParameter() {
        Condition condition = new Condition();
        condition.setCondition("get");
        condition.setFilteringField(FILTERING_FIELD);
        condition.setValue("4");
        List<Condition> conditions = new ArrayList<>();
        conditions.add(condition);

        Order order1 = new Order();
        order1.setIsAsc(false);
        order1.setSortingColumn(SORTING_COLUMN);
        List<Order> orders = new ArrayList<>();
        orders.add(order1);

        return new FilterBuilder()
                .setName(CHANGED_NAME + "_" + Instant.now().getEpochSecond())
                .setType(TYPE)
                .setShare(false)
                .setDescription(DESCRIPTION)
                .setConditions(conditions)
                .setOrders(orders)
                .build();
    }
    public static Filter getProductBugFilterChanged() {
        Condition condition = new Condition();
        condition.setCondition("gte");
        condition.setFilteringField(FILTERING_FIELD);
        condition.setValue("4");
        List<Condition> conditions = new ArrayList<>();
        conditions.add(condition);

        Order order1 = new Order();
        order1.setIsAsc(false);
        order1.setSortingColumn(SORTING_COLUMN);
        List<Order> orders = new ArrayList<>();
        orders.add(order1);

        return new FilterBuilder()
                .setName(CHANGED_NAME + "_" + Instant.now().getEpochSecond())
                .setId(filterId)
                .setType(TYPE)
                .setShare(false)
                .setDescription(DESCRIPTION)
                .setConditions(conditions)
                .setOrders(orders)
                .build();
    }

    public static Filter getFilterWithSpecifiedId(int id) {
        Condition condition = new Condition();
        condition.setCondition("gte");
        condition.setFilteringField(FILTERING_FIELD);
        condition.setValue("4");
        List<Condition> conditions = new ArrayList<>();
        conditions.add(condition);

        Order order1 = new Order();
        order1.setIsAsc(false);
        order1.setSortingColumn(SORTING_COLUMN);
        List<Order> orders = new ArrayList<>();
        orders.add(order1);

        return new FilterBuilder()
                .setName(CHANGED_NAME + "_" + Instant.now().getEpochSecond())
                .setId(id)
                .setType(TYPE)
                .setShare(false)
                .setDescription(DESCRIPTION)
                .setConditions(conditions)
                .setOrders(orders)
                .build();
    }

    public static Filter getFilterWithMissingOrder() {
        Condition condition = new Condition();
        condition.setCondition("gte");
        condition.setFilteringField(FILTERING_FIELD);
        condition.setValue("4");
        List<Condition> conditions = new ArrayList<>();
        conditions.add(condition);

        return new FilterBuilder()
                .setName("FilterWithMissingOrder" + "_" + Instant.now().getEpochSecond())
                .setType(TYPE)
                .setShare(false)
                .setDescription(DESCRIPTION)
                .setConditions(conditions)
                .build();

    }
}
