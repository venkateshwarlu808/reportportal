package com.epam.mentoring.advanced.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.FindBys;

import java.util.List;

public class FiltersPage extends BasePage {

    public FiltersPage(WebDriver driver) {
        super(driver);
    }
    @FindBy(css = "[title='Filters']")
    private WebElement filtersTitle;

    @FindBy(xpath = "//input[@class='inputSearch__input--3e4db type-text']")
    private WebElement searchField;

    @FindBy(xpath = "//div[@class='itemCounter__item-counter--5RYv5']")
    private WebElement quantityOfFilters;

    @FindBy(css = "[class='ghostButton__text--eUa9T']")
    private WebElement addFilterButton;

    @FindBys(@FindBy(xpath = "//span[@class='filterName__name--3eMTq filterName__bold--11M-1 filterName__link--2GS27']"))
    private List<WebElement> filtersNames;

    @FindAll(@FindBy(xpath = "//span[@class='filterName__pencil-icon--1dKe8']"))
    private List<WebElement> pencilsForFilters;
    @FindAll(@FindBy(xpath = "//p[@class='filterOptions__filter-options--2DJe8']"))
    private List<WebElement> filterOptions;
    @FindBy(xpath = "//div[@class='shareFilter__shared-icon--1yvGF']//*[local-name()='svg']")
    private WebElement sharedTick;
    @FindAll(@FindBy(xpath = "//span[@class='inputSwitcher__children-container--1zvZY']"))
    private List<WebElement> displayOnLaunchesSwitchers;
    @FindAll(@FindBy(xpath = "//span[@class='displayFilter__switcher-label--WJfP6']"))
    private List<WebElement> displayOnLaunchesValues;

    @FindAll(@FindBy(xpath = "//div[@class='deleteFilterButton__bin-icon--3aWi9']"))
    private List<WebElement> deleteButtons;

    //Edit filter window
    @FindBy(xpath = "//input[@placeholder='Enter filter name']")
    private WebElement editFilterNameField;
    @FindBy(xpath = "//span[@role='presentation']")
    private WebElement editFilterDescriptionField;
    @FindBy(xpath = "//button[@class='bigButton__big-button--ivY7j bigButton__color-booger--2IfQT']")
    private WebElement editFilterUpdateButton;
    @FindBy(xpath = "//div[@class='inputBigSwitcher__slider--1dQsG inputBigSwitcher__turned-on--3MTlj']")
    private WebElement turnedOnSharing;
    @FindBy(xpath = "//div[@class='inputBigSwitcher__slider--1dQsG']")
    private WebElement turnedOffSharing;
    @FindBy(xpath = "//div[@class='inputBigSwitcher__switcher-wrapper--2-dur']")
    private WebElement sharingSwitcher;


    public void searchForFilter(String filterName) {
        searchField.sendKeys(filterName);
        waiters.waitingForTextToBePresent(quantityOfFilters, "1 - 1 of 1");
    }
    public boolean verifyFilterOptions(String filterName, String filterOptionName, String condition, String value) {
        String actualText = findFilterElement(filtersNames, filterOptions, filterName).getText();
        actualText = actualText.substring(actualText.indexOf("(") + 1, actualText.indexOf(")"));
        String expectedText = filterOptionName;
        switch (condition) {
            case "Equals":
                expectedText += " = ";
                break;
            case "Greater than or equal":
                expectedText += " >= ";
                break;
            case "Less than or equal":
                expectedText += " <= ";
                break;
            case "Not equals":
                expectedText += " != ";
                break;
            default:
                expectedText += " " + condition + " ";
                break;
        }
        expectedText += value;
        logger.info("Expected result: {}", expectedText);
        logger.info("Actual result: {}", actualText);
        return actualText.equalsIgnoreCase(expectedText);
    }

    public void renameSelectedFilter(String newFilterName) {
        editFilterNameField.clear();
        editFilterNameField.sendKeys(newFilterName);
        editFilterUpdateButton.click();
        waiters.waitingForElementToBeGone(editFilterUpdateButton);
    }

    public void deleteFilterFromTheList(String filterName) {
        if (isFilterInTheList(filterName)) {
            findFilterElement(filtersNames, deleteButtons, filterName).click();
            waiters.waitingForElementToBeClickable(confirmDeleteButton);
            confirmDeleteButton.click();
            waiters.waitingForElementToBeGone(confirmDeleteButton);
        }
    }

    public boolean isFilterInTheList(String filterName) {
        logger.info("We are looking for filter: {}", filterName);
        for (WebElement filtersName:
                filtersNames) {
            if(filtersName.getText().contentEquals(filterName)) {
                logger.info("Filter {} is in the list", filterName);
                return true;
            }
        }
        logger.info("There is no such filter in the list: {}", filterName);
        return false;
    }
    public void clickAddNewFilterButton() {
        logger.info("Add new filter");
        waiters.waitingForElementToBeClickable(addFilterButton);
        addFilterButton.click();
        waiters.waitingForElementToBeGone(addFilterButton);
    }
    public void toggleDisplayOnLaunches(String filterName, boolean value) {
        if (findFilterElement(filtersNames, displayOnLaunchesValues, filterName).getText().contentEquals("ON") && value
                || findFilterElement(filtersNames, displayOnLaunchesValues, filterName).getText().contentEquals("OFF") && !value) {
            logger.info("Values of Display Launches are the same");
        } else {
            findFilterElement(filtersNames, displayOnLaunchesSwitchers, filterName).click();
            logger.info("Toggling Display on Launches for filter {} with value {}", filterName, value);
        }
    }

    public boolean isSharedTickDisplayed() {
        return isElementDisplayed(sharedTick);
    }
    public void toggleSharing(boolean expectedSharing) {
        if (isElementDisplayed(turnedOnSharing) && expectedSharing ||
                isElementDisplayed(turnedOffSharing) && !expectedSharing) {
            logger.info("Toggle is already: {}", expectedSharing);

        } else {
            logger.info("Sharing toggle is clicked");
            sharingSwitcher.click();
        }
        editFilterUpdateButton.click();
        waiters.waitingForElementToBeDisplayed(notificationForUpdateFilter);
        waiters.waitingForElementToBeGone(notificationForUpdateFilter);
    }
    public void editFilter(String filterName)  {
        action.moveToElement(findFilterElement(filtersNames, filtersNames, filterName)).perform();
        findFilterElement(filtersNames, pencilsForFilters, filterName).click();
        logger.info("Editing filter {}", filterName);
    }
    public boolean isUserOnFiltersPage() {
        waiters.waitingForElementToBeDisplayed(filtersTitle);
        logger.info("Hooray, we are on the Filters page.");
        return filtersTitle.isDisplayed();
    }

    public WebElement findFilterElement(List<WebElement> elementsNamesList, List<WebElement> elementsList, String filterName) {
        logger.info("Looking for element in {} for the following filter: {}", elementsList, filterName);
        int indexOfElement = 0;
        for (WebElement filtersName:
                elementsNamesList) {
            if (filtersName.getText().contentEquals(filterName)) {
                indexOfElement = elementsNamesList.indexOf(filtersName);
                logger.info("There is a match! Here is the index of the element: {}", indexOfElement);
            }
        }
        logger.info("Returned the following element: {}", elementsList.get(indexOfElement));
        return elementsList.get(indexOfElement);
    }

    public boolean isElementDisplayed(WebElement element) {
        try {
            waiters.waitingForElementToBeDisplayed(element);
            return element.isDisplayed();
        } catch(Exception e) {
            logger.info("Element {} is not displayed.",  element);
            return false;
        }
    }


}
