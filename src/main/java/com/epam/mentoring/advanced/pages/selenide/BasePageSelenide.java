package com.epam.mentoring.advanced.pages.selenide;

import com.codeborne.selenide.SelenideElement;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import static com.codeborne.selenide.Selenide.$;

public class BasePageSelenide {
    public static final Logger LOGGER = LogManager.getLogger(BasePageSelenide.class);
    protected SelenideElement confirmDeleteButton = $("button.bigButton__big-button--ivY7j.bigButton__color-tomato--Wvy5L");
}
