package com.epam.mentoring.advanced.pages.selenide;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.*;

public class FiltersPageSelenide extends BasePageSelenide {
    private final SelenideElement filtersTitle = $("[title='Filters']");
    private final SelenideElement searchField = $("input.inputSearch__input--3e4db.type-text");
    private final SelenideElement quantityOfFilters = $("div.itemCounter__item-counter--5RYv5");
    private final SelenideElement addFilterButton = $(".ghostButton__text--eUa9T");
    private final ElementsCollection filtersNames = $$("span.filterName__name--3eMTq.filterName__bold--11M-1.filterName__link--2GS27");
    private final ElementsCollection pencilsForFilters = $$("span.filterName__pencil-icon--1dKe8");
    private final ElementsCollection filterOptions = $$("p.filterOptions__filter-options--2DJe8");
    private final ElementsCollection displayOnLaunchesSwitchers = $$("span.inputSwitcher__children-container--1zvZY");
    private final ElementsCollection displayOnLaunchesValues = $$("span.displayFilter__switcher-label--WJfP6");
    private final ElementsCollection deleteButtons = $$("div.deleteFilterButton__bin-icon--3aWi9");
    private final SelenideElement editFilterNameField = $("input[placeholder='Enter filter name']");
    private final SelenideElement editFilterUpdateButton = $("button.bigButton__big-button--ivY7j.bigButton__color-booger--2IfQT");

    public void searchForFilter(String filterName) {
        searchField.sendKeys(filterName);
        searchField.pressEnter();
        quantityOfFilters.shouldHave(text("1 - 1 of 1"));
    }

    public boolean verifyFilterOptions(String filterName, String filterOptionName, String condition, String value) {
        filtersNames.find(text(filterName)).should(appear);
        LOGGER.info("FindBy: {}", filtersNames.find(text(filterName)));
        int index = filtersNames.indexOf(filtersNames.find(text(filterName)));
        LOGGER.info("INDEX: {}", index);
        String actualText = filterOptions.get(index).text().substring(filterOptions.get(index).text().indexOf("(") + 1, filterOptions.get(index).text().indexOf(")"));

        String expectedText = filterOptionName;
        switch (condition) {
            case "Equals":
                expectedText += " = ";
                break;
            case "Greater than or equal":
                expectedText += " >= ";
                break;
            case "Less than or equal":
                expectedText += " <= ";
                break;
            case "Not equals":
                expectedText += " != ";
                break;
            default:
                expectedText += " " + condition + " ";
                break;
        }
        expectedText += value;
        LOGGER.info("Expected result: {}", expectedText);
        LOGGER.info("Actual result: {}", actualText);
        return actualText.equalsIgnoreCase(expectedText);
    }

    public void renameSelectedFilter(String newFilterName) {
        editFilterNameField.clear();
        editFilterNameField.sendKeys(newFilterName);
        editFilterUpdateButton.click();
        editFilterUpdateButton.should(disappear);
    }

    public void deleteFilterFromTheList(String filterName) {

        findFilterElement(filtersNames, deleteButtons, filterName).click();
        confirmDeleteButton.should(appear);
        confirmDeleteButton.click();
        confirmDeleteButton.should(disappear);
        filtersNames.find(text(filterName)).should(disappear);


    }
    public boolean isFilterInTheList(String filterName) {

        LOGGER.info("isFilterInTheList is called");
        LOGGER.info("isVisible? {}", filtersNames.find(text(filterName)).is(visible));

        return filtersNames.find(text(filterName)).is(visible);

    }

    public void clickAddNewFilterButton() {
        LOGGER.info("Add new filter");
        addFilterButton.shouldBe(visible);
        addFilterButton.click();
        addFilterButton.should(disappear);
    }

    public void toggleDisplayOnLaunches(String filterName, boolean value) {
        if ((findFilterElement(filtersNames, displayOnLaunchesValues, filterName).getText().equals("ON") && value)
                || (findFilterElement(filtersNames, displayOnLaunchesValues, filterName).getText().equals("OFF") && !value)) {
            LOGGER.info("Values of Display Launches are the same");
        } else {
            findFilterElement(filtersNames, displayOnLaunchesSwitchers, filterName).click();
            LOGGER.info("Toggling Display on Launches for filter {} with value {}", filterName, value);
        }
    }


    public void editFilter(String filterName) {
        actions().moveToElement(findFilterElement(filtersNames, filtersNames, filterName)).perform();
        findFilterElement(filtersNames, pencilsForFilters, filterName).click();
        LOGGER.info("Editing filter {}", filterName);
    }

    public boolean isUserOnFiltersPage() {
        filtersTitle.should(appear);
        LOGGER.info("Hooray, we are on the Filters page.");
        return filtersTitle.isDisplayed();
    }

    public SelenideElement findFilterElement(ElementsCollection elementsNamesList, ElementsCollection elementsList, String filterName) {
        int indexOfElement = 0;
        SelenideElement element = elementsNamesList.find(text(filterName)).should(appear);
        indexOfElement = elementsNamesList.indexOf(element);
        return elementsList.get(indexOfElement);
    }

    public boolean isElementDisplayed(SelenideElement element) {
        try {
            element.should(appear);
            return element.isDisplayed();
        } catch (Exception e) {
            LOGGER.info("Element {} is not displayed", element);
            return false;
        }
    }
}
