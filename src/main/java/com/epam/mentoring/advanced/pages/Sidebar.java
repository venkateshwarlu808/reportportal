package com.epam.mentoring.advanced.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class Sidebar extends BasePage {
    public Sidebar(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "//div[@class='layout__sidebar-container--gX2bY']//a[contains(@href, '/filters')]")
    private WebElement filtersSidebarButton;
    @FindBy(xpath = "//div[@class='layout__sidebar-container--gX2bY']//a[contains(@href, '/launches')]")
    private WebElement launchesSidebarButton;
    @FindBy(xpath = "//div[@class='layout__sidebar-container--gX2bY']//a[contains(@href, '/dashboard')]")
    private WebElement dashboardsSidebarButton;
    @FindBy(xpath = "//div[@class='layout__sidebar-container--gX2bY']//a[contains(@href, '/settings')]")
    private WebElement settingsSidebarButton;
    @FindBy(xpath = "//div[@class='allLatestDropdown__selected-value--WN9ZG']")
    private WebElement allLaunches;

    @FindBy(css = "[title='Filters']")
    private WebElement filtersTitle;



    public void goToFiltersPage() {
        BasePage.logger.info("Going to Filters Page");
        waiters.waitingForElementToBeClickable(filtersSidebarButton);
        filtersSidebarButton.click();
        waiters.waitingForElementToBeDisplayed(filtersTitle);
    }
    public void goToLaunchesPage() {
        BasePage.logger.info("Going to Launches Page");
        waiters.waitingForElementToBeClickable(launchesSidebarButton);
        launchesSidebarButton.click();
        waiters.waitingForElementToBeDisplayed(allLaunches);
    }

    public void goToSettingsPage() {
        BasePage.logger.info("Going to Settings Page");
        waiters.waitingForElementToBeClickable(settingsSidebarButton);
        settingsSidebarButton.click();
    }

    public void goToDashboardsPage() {
        BasePage.logger.info("Going to Dashboards Page");
        waiters.waitingForElementToBeClickable(dashboardsSidebarButton);
        dashboardsSidebarButton.click();
    }

}
