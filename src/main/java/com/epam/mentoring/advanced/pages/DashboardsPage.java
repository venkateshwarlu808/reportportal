package com.epam.mentoring.advanced.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class DashboardsPage extends BasePage {

    public DashboardsPage(WebDriver driver) {
        super(driver);
    }

    @FindAll(@FindBy(xpath = "//a[@class='gridCell__grid-cell--3e2mS gridCell__align-left--2beIG dashboardTable__name--1sWJs']"))
    private List<WebElement> dashboards;

    @FindAll(@FindBy(xpath = "//i[@class='icon__icon--2m6Od icon__icon-delete--1jIHF']"))
    private List<WebElement> deleteDashboardButtons;

    public boolean checkIfDashboardPresent(String dashboardName) {
        if (dashboards.isEmpty()) {
            logger.info("There is no dashboards in the list");
            return false;
        } else {
            for (WebElement dashboard :
                    dashboards) {
                if (dashboard.getText().contentEquals(dashboardName)) {
                    logger.info("There is a match for dashboard: {}", dashboardName);
                    return true;
                }
            }
        }
        return false;
    }

    public void deleteDashboard(String dashboardName) {
        if (dashboards.isEmpty()) {
            logger.error("There is no dashboards in the list");
        } else {
            for (WebElement dashboard :
                    dashboards) {
                if (dashboard.getText().contentEquals(dashboardName)) {
                    deleteDashboardButtons.get(dashboards.indexOf(dashboard)).click();
                    waiters.waitingForElementToBeClickable(confirmDeleteButton);
                    confirmDeleteButton.click();
                }
            }
        }
    }
}
