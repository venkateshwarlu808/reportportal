package com.epam.mentoring.advanced.pages.selenide;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebDriver;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.*;
import static com.codeborne.selenide.WebDriverRunner.getWebDriver;

public class DashboardsPageSelenide extends BasePageSelenide {

    private final ElementsCollection dashboards = $$("a.dashboardTable__name--1sWJs");
    public final SelenideElement dashboardTitle = $("li.pageBreadcrumbs__page-breadcrumbs-item--1GzrN span");

    private final ElementsCollection dashboardWidgets = $$("div.react-grid-item.widgetsGrid__widget-view--dVnmj");
    private final ElementsCollection dashboardWidgetsHeaders = $$("div.widget__widget-header--eR4Gu");
    private final ElementsCollection resizer = $$("span.react-resizable-handle.react-resizable-handle-se");

    private Dimension initialSize = null;
    private Dimension changedSize = null;

    private Point initialLocation = null;
    private Point changedLocation = null;




    public void selectDashboardFromTheList(String dashboardName) {
        dashboards.find(exactText(dashboardName)).click();
        LOGGER.info("Selected {} dashboard", dashboardName);
    }

    public void changeWidgetSize(String widgetName, int xOffset, int yOffset) {
        initialSize = dashboardWidgets.find(text(widgetName)).getSize();
        LOGGER.info("Initial widget size: {}", initialSize);


        initialLocation = dashboardWidgets.get(dashboardWidgets.indexOf(dashboardWidgets.find(text(widgetName))) + 1).getLocation();
        LOGGER.info("Initial neighbour widget location: {}", initialLocation);

        if (!dashboardWidgets.findBy(text(widgetName)).isDisplayed()) {
            LOGGER.info("Widget {} is not visible, performing scrollIntoView", widgetName);
            dashboardWidgets.findBy(text(widgetName)).scrollIntoView(true);
            LOGGER.info("Widget {}  is now in the view", widgetName);
        }

        LOGGER.info("Perform hover over widget {}", widgetName);
        dashboardWidgetsHeaders.findBy(text(widgetName)).
                hover();

        LOGGER.info("Perform move resizer by xOffset: {} and yOffset: {}", xOffset, yOffset);
        actions().
                clickAndHold(resizer.findBy(visible)).
                moveByOffset(xOffset ,yOffset).
                release().
                perform();

        changedSize = dashboardWidgets.findBy(text(widgetName)).getSize();
        LOGGER.info("Changed widget size {}", changedSize);

        changedLocation = dashboardWidgets.get(dashboardWidgets.indexOf(dashboardWidgets.findBy(text(widgetName)))).getLocation();
        LOGGER.info("Changed neighbour widget location: {}", changedLocation);

        dashboardWidgets.findBy(text(widgetName)).
                hover();
    }

    public boolean isWidgetSizeChanged() {
        if (initialSize != null && changedSize != null) {
            if (!initialSize.equals(changedSize)) {
                LOGGER.info("Initial size of the widget: {}", initialSize);
                LOGGER.info("Changed size of the widget: {}", changedSize);
                LOGGER.info("Widget size has changed!");
                initialSize = null;
                changedSize = null;
                return true;
            } else {
                LOGGER.info("Widget size has not changed.");
                initialSize = null;
                changedSize = null;
                return false;
            }
        } else {
            LOGGER.error("Cannot verify size change. Initial or changed size is null.");
            initialSize = null;
            changedSize = null;
            return false;
        }
    }

    public void moveWidget(String widgetName, int xOffset, int yOffset) {
        initialLocation = dashboardWidgets.findBy(text(widgetName)).getLocation();

        LOGGER.info("Initial widget location: {}", initialLocation);
        actions()
                .dragAndDropBy(dashboardWidgetsHeaders.findBy(text(widgetName)), xOffset, yOffset)
                .perform();

        changedLocation = dashboardWidgets.findBy(text(widgetName)).getLocation();

        LOGGER.info("Changed widget location: {}", changedLocation);
    }

    public boolean isWidgetMoved() {
        if (initialLocation != null && changedLocation != null) {
            if (!initialLocation.equals(changedLocation)) {
                LOGGER.info("Initial location of the widget: {}", initialLocation);
                LOGGER.info("Changed location of the widget: {}", changedLocation);
                LOGGER.info("Widget location has changed!");
                initialLocation = null;
                changedLocation = null;
                return true;
            } else {
                LOGGER.info("Widget location has not changed.");
                initialLocation = null;
                changedLocation = null;
                return false;
            }
        } else {
            LOGGER.error("Cannot verify location change. Initial or changed location is null.");
            initialLocation = null;
            changedLocation = null;
            return false;
        }
    }

    //    JS executor task
    public void scrollToElement(SelenideElement element) {
        WebDriver driver = getWebDriver();
        String script = "arguments[0].scrollIntoView(true);";
        ((JavascriptExecutor) driver).executeScript(script, element);
    }
    public void clickWithJavaScript(SelenideElement element) {
        WebDriver driver = getWebDriver();
        String script = "arguments[0].click();";
        ((JavascriptExecutor) driver).executeScript(script, element);
    }
    public boolean isElementScrolledIntoView(SelenideElement element) {
        WebDriver driver = getWebDriver();
        String script = "var rect = arguments[0].getBoundingClientRect();" +
                "return (rect.top >= 0 && rect.bottom <= window.innerHeight);";
        return (boolean) ((JavascriptExecutor) driver).executeScript(script, element);
    }
}

