package com.epam.mentoring.advanced.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class SettingsPage extends BasePage {
    public SettingsPage(WebDriver driver) {
        super(driver);
    }
    @FindBy(xpath = "//a[@class='navigationTabs__tab--VRU69' and contains(@href, '/demoData')]")
    private WebElement demoDataTab;

    @FindBy(xpath = "//span[@class='generateDemoDataBlock__generate-button-title--1peid']")
    private WebElement generateDemoDataButton;

    @FindBy(xpath = "//div[@class='generateDemoDataBlock__preloader-info--3fNim']")
    private WebElement generateDemoDataLoader;

    public void generateDemoData() {
        waiters.waitingForElementToBeClickable(demoDataTab);
        demoDataTab.click();
        waiters.waitingForElementToBeClickable(generateDemoDataButton);
        generateDemoDataButton.click();
        waiters.waitingForElementToBeGone(generateDemoDataLoader);
        waiters.waitingForElementToBeDisplayed(notificationForDemoData);
        waiters.waitingForElementToBeGone(notificationForDemoData);
    }

}
