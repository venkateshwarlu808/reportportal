package com.epam.mentoring.advanced.managers;

import com.codeborne.selenide.Configuration;
import com.epam.mentoring.advanced.config.EnvConfig;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.aeonbits.owner.ConfigFactory;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.MutableCapabilities;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.edge.EdgeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.opera.OperaOptions;

import java.util.concurrent.TimeUnit;

public class DriverManager {
    private WebDriver driver;
    protected static final EnvConfig envConfig = ConfigFactory.create(EnvConfig.class);
    private static final String SELENOID_URL = "http://localhost:4444/wd/hub";
    private static final String SAUCE_LABS_URL = "https://ondemand.eu-central-1.saucelabs.com:443/wd/hub";
    private static String testName;

    public static void setupSelenideDriver() {
        Configuration.browser = envConfig.browser();
        Configuration.driverManagerEnabled = true;
        Configuration.headless = envConfig.headless();
        Configuration.timeout = 10000;
        Configuration.pageLoadTimeout = 10000;

        switch (envConfig.browser()) {
            case "firefox":
                WebDriverManager.firefoxdriver().setup();
                break;
            case "opera":
                WebDriverManager.operadriver().setup();
                break;
            default:
                WebDriverManager.chromedriver().setup();
                break;
        }
        if (envConfig.headless().equals(true)) {
            Configuration.browserSize = "2560x1600";
        } else {
            Configuration.startMaximized = true;
        }
    }
    private WebDriver setupSeleniumDriver() {
        switch (envConfig.browser()) {
            case "firefox":
                WebDriverManager.firefoxdriver().setup();
                FirefoxOptions firefoxOptions = new FirefoxOptions();
                firefoxOptions.setHeadless(envConfig.headless());
                driver = new FirefoxDriver(firefoxOptions);
                break;
            case "edge":
                WebDriverManager.edgedriver().setup();
                EdgeOptions edgeOptions = new EdgeOptions();
                driver = new EdgeDriver(edgeOptions);
                break;
            default:
                WebDriverManager.chromedriver().setup();
                ChromeOptions chromeOptions = new ChromeOptions();
                chromeOptions.setHeadless(envConfig.headless());
                driver = new ChromeDriver(chromeOptions);
                break;
        }

        if (envConfig.headless().equals(true)) {
            driver.manage().window().setSize(new Dimension(2560, 1600));
        } else {
            driver.manage().window().maximize();
        }

        driver.manage().timeouts().pageLoadTimeout(10, TimeUnit.SECONDS);
        driver.manage().timeouts().implicitlyWait(2, TimeUnit.SECONDS);

        return driver;
    }

    public static void setupSelenoidDriver() {
        MutableCapabilities selenoidOptions = new MutableCapabilities();
        selenoidOptions.setCapability("enableVNC", true);
        selenoidOptions.setCapability("enableVideo", true);

        MutableCapabilities capabilities = initCapabilities();
        capabilities.setCapability("selenoid:options", selenoidOptions);

        Configuration.browserCapabilities = capabilities;
        Configuration.remote = SELENOID_URL;
    }

    public static void setupSauceLabsDriver() {
        MutableCapabilities sauceOptions = new MutableCapabilities();
        sauceOptions.setCapability("screenResolution", "2560x1600");
        sauceOptions.setCapability("username", envConfig.sauceUser());
        sauceOptions.setCapability("accessKey", envConfig.sauceKey());
        sauceOptions.setCapability("name", testName);

        MutableCapabilities capabilities = initCapabilities();
        capabilities.setCapability("platformName", envConfig.platformName());
        capabilities.setCapability("browserVersion", envConfig.browserVersion());
        capabilities.setCapability("sauce:options", sauceOptions);

        Configuration.browserCapabilities = capabilities;
        Configuration.browser = envConfig.browser();
        Configuration.driverManagerEnabled = true;
        Configuration.startMaximized = true;
        Configuration.remote = SAUCE_LABS_URL;
    }

    public static MutableCapabilities initCapabilities() {
        MutableCapabilities capabilities;
        switch (envConfig.browser()) {
            case "firefox":
                capabilities = new FirefoxOptions();
                WebDriverManager.firefoxdriver().setup();
                break;
            case "opera":
                capabilities = new OperaOptions();
                WebDriverManager.operadriver().setup();
                break;
            case "edge":
                capabilities = new EdgeOptions();
                WebDriverManager.edgedriver().setup();
                break;
            default:
                capabilities = new ChromeOptions();
                WebDriverManager.chromedriver().setup();
                break;
        }
        return capabilities;
    }
    public WebDriver getDriver() {
        if(driver == null) driver = setupSeleniumDriver();
        return driver;
    }
    public void closeDriver() {
        driver.close();
        driver.quit();
    }
    public static void setTestName(String name) {
        testName = name;
    }
}
