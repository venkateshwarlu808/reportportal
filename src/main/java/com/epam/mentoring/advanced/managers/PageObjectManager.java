package com.epam.mentoring.advanced.managers;

import com.epam.mentoring.advanced.pages.*;
import org.openqa.selenium.WebDriver;

public class PageObjectManager {
    private final WebDriver driver;
    private DashboardsPage dashboardsPage;
    private FiltersPage filtersPage;
    private LaunchesPage launchesPage;
    private LoginPage loginPage;
    private SettingsPage settingsPage;
    private Sidebar sidebar;

    public PageObjectManager(WebDriver driver) {
        this.driver = driver;
    }

    public DashboardsPage getDashboardsPage() {
        if (dashboardsPage == null) {
            dashboardsPage = new DashboardsPage(driver);
        }
        return dashboardsPage;
    }

    public FiltersPage getFiltersPage() {
        if (filtersPage == null) {
            filtersPage = new FiltersPage(driver);
        }
        return filtersPage;
    }

    public LaunchesPage getLaunchesPage() {
        if (launchesPage == null) {
            launchesPage = new LaunchesPage(driver);
        }
        return launchesPage;
    }

    public LoginPage getLoginPage() {
        if (loginPage == null) {
            loginPage = new LoginPage(driver);
        }
        return loginPage;
    }

    public SettingsPage getSettingsPage() {
        if (settingsPage == null) {
            settingsPage = new SettingsPage(driver);
        }
        return settingsPage;
    }

    public Sidebar getSidebar() {
        if (sidebar == null) {
            sidebar = new Sidebar(driver);
        }
        return sidebar;
    }
}
