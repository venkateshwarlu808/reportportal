package com.epam.mentoring.advanced.app.bo;

public interface DiscountUtility {
    double calculateDiscount(UserAccount userAccount);
}
